const site = [

    [
        "projet",
        "https://trello.com/",
        "Trello" 
    ],

    [
        "git",
        "https://gitlab.com/",
        "GitLab" 
    ],

    [
        "git",
        "https://framagit.org/",
        "Framagit" 
    ],

    [
        "git",
        "https://github.com/",
        "GitHub" 
    ],

    [
        "projet",
        "https://www.squarespace.com/logo#N4IghgrgLgFgpgExALgGZgDYGc4F8gAA",
        "LOGO create squarespace" 
    ],

    [
        "projet",
        "https://www.iloveimg.com/fr/compresser-image",
        "IloveIMG" 
    ],

    [
        "design",
        "https://lottiefiles.com/",
        "Lottie Animations image" 
    ],

    [
        "design",
        "https://storyset.com/",
        "Storyset Motion images" 
    ],

    [
        "projet",
        "https://www.freepik.com/",
        "FreePik free images" 
    ],

    [
        "color",
        "https://coolors.co/",
        "Coolors" 
    ],

    [
        "design",
        "www.figma.com",
        "Figma" 
    ],

    [
        "color",
        "https://color.adobe.com/fr/create/color-wheel",
        "Color Adobe" 
    ],

    [
        "code",
        "https://cssgradient.io/",
        "CSS Gradient" 
    ],

    [
        "code",
        "http://localhost/",
        "Localhost" 
    ],

    [
        "code",
        "http://localhost/phpmyadmin/index.php",
        "PhpMyAdmin" 
    ],

    [
        "projet",
        "https://www.google.com/intl/fr-CA/docs/about/",
        "Google Doc" 
    ],

    [
        "design",
        "https://iconify.design/",
        "Iconify" 
    ],

    [
        "code",
        "https://getbootstrap.com/",
        "Bootstrap 5" 
    ],

    [
        "code",
        "https://www.gradient-animator.com/",
        "CSS Gradient Animator" 
    ],

    [
        "design",
        "https://webgradients.com/",
        "Web Gradients" 
    ],

    [
        "code",
        "https://osintframework.com/",
        "OSINT FRAMEWORK" 
    ],

    [
        "color",
        "color-block-generator/index.html",
        "Color Generator By Jessy" 
    ],

    [
        "color",
        "color-gradient-generator/index.html",
        "Color Generator By Jessy" 
    ],

    [
        "code",
        "http://localhost/",
        "Color Gradient Generator By Jessy" 
    ],

    [
        "projet",
        "https://app.usepanda.com/#/",
        "App Panda" 
    ],

    [
        "projet",
        "https://onepagelove.com/",
        "OnePageLove" 
    ],

    [
        "design",
        "https://passionhacks.com/gradient-isometric-illustrations/",
        "Passion Hack Illustrations" 
    ],

    [
        "design",
        "https://9elements.github.io/fancy-border-radius/full-control.html#0.36.0.0-100.76.91.0-.",
        "Fancy Border Radius" 
    ],

    [
        "design",
        "https://www.blobmaker.app/",
        "Blob Maker" 
    ],

    [
        "design",
        "https://getwaves.io/",
        "Get Waves" 
    ],

    [
        "code",
        "https://snowfall-in-december.netlify.app/",
        "Snowfall in December" 
    ],

    [
        "code",
        "https://www.react-simple-maps.io/",
        "Map monde SVG" 
    ],

    [
        "design",
        "https://app.haikei.app/",
        "Fond svg Generator" 
    ],

    [
        "design",
        "https://graphiste.com/blog/30-animations-de-texte-pour-surprendre-vos-visiteurs",
        "Graphiste.com" 
    ],

    [
        "design",
        "https://freefrontend.com/css-animation-examples/",
        "FreeFrontEnd" 
    ],

    [
        "design",
        "http://vivify.mkcreative.cz/",
        "Vivify" 
    ],

    [
        "code",
        "https://www.mockplus.com/blog/post/css-animation-examples",
        "MockPlus" 
    ],

    [
        "design",
        "https://bashooka.com/coding/40-cool-css-animation-examples/",
        "Bashooka" 
    ],

    [
        "design",
        "https://www.cssscript.com/best-particles-animation/",
        "Particle Effect" 
    ],

    [
        "projet",
        "https://www.subdelirium.com/generateur-de-mentions-legales/",
        "Mentions légales" 
    ],

    [
        "projet",
        "https://www.splitshire.com/",
        "Splitshire" 
    ],

    [
        "projet",
        "https://pixabay.com/fr/",
        "Pixabay" 
    ],

    [
        "projet",
        "https://wallhere.com/",
        "Wallhere" 
    ],

    [
        "projet",
        "https://mixkit.co/free-video-backgrounds/",
        "Mixkit Video Backgrounds" 
    ],

    [
        "projet",
        "https://fr.videezy.com/video-libre/motion-background",
        "Videezy" 
    ],

    [
        "projet",
        "https://html5up.net/",
        "Html5up Site Template" 
    ],

    [
        "code",
        "https://htmlcolorcodes.com/",
        "Html color codes" 
    ],

    [
        "code",
        "https://fr.tuto.com/blog/2019/10/16-snippets-css-javascript-gratuits.htm",
        "Anim js code" 
    ],

    [
        "code",
        "https://www.shodan.io/dashboard",
        "Shodan Cam" 
    ],

];

const section = document.getElementById('content-link')

function genereLink(){

    for(i = 0; i < site.length; i++){

        var div = document.createElement('div');
        div.classList = 'div-link';
        div.dataset.projet = site[i][0];

        var a = document.createElement('a');
        a.target = "_blank";
        a.href = site[i][1];
        a.innerText = site[i][2];

        div.appendChild(a);
        section.appendChild(div);

    }

}

genereLink();